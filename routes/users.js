const router = require('koa-router')()
import passport from '../passport'
import Oauth2orize from 'oauth2orize'
const oauth2orize=new Oauth2orize()
import User from '../data_base/models/user_model'
import jwt from 'jsonwebtoken'
const localAuth = async (ctx) => await passport.authenticate('local', {session: false},
    (req,user) =>{
        if (user){
            ctx.body=user;
        }
        else{
            ctx.body='';
        }
        //res.body=(!user)?'Incorrect':user.email
    });

router.prefix('/users')
router.post('/registration', async function(ctx) {
    const email = ctx.request.header.email;
    const userName = ctx.request.header.username;
    const password = ctx.request.header.password;
    if ((await User.query().where('email', email))[0] instanceof User) {
        ctx.body = 'Fail: email is used';
    }
    else {
        if ((await User.query().where('userName', userName))[0] instanceof User){
            ctx.body = 'Fail: username is used';
        }
        else {
            await User.registration(email,userName,password);
            ctx.body = jwt.sign({email}, 'secret')//ошибка
        }
    }
})
router.get('/isUser', async function (ctx) {
    const email = ctx.url.slice(ctx.url.indexOf('email')+6, ctx.url.length);
    ctx.body = await User.isUser(email); //если есть юзер, то возвращаем jwt, если нет, то null
})

router.post('/login', async (ctx) => await passport.authenticate('local', {session: false},
 (req,user) =>{
     if (user){
         ctx.body=user;
     }
     else{
         ctx.body='';
     }
  })
(ctx))

router.post('/jwt', async ctx =>
    await passport.authenticate('jwt', { session: false },
      (error, user) => {
        ctx.body=(!user)?'Incorrect':'Correct'
      })(ctx)
)

router.get('/auth/google',
    passport.authenticate('google', { scope: ['profile'] }));

router.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });

    router.get('/vkontakte',
    passport.authenticate('vkontakte'),
    function(req, res){x
        // The request will be redirected to vk.com for authentication, so
        // this function will not be called.
    });

router.get('/vkontakte/callback',
    passport.authenticate('vkontakte', { failureRedirect: '/login' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/users/bar');
    });
export default router