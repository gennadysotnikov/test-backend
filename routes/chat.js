import {
    createChat,
    findMassages, getChatMassages,
    getChatsFromChat,
    getChatsFromParty,
    inviteToChat,
    sendMassage
} from "../data_base/queries"
import passport from "../passport";
import User from "../data_base/models/user_model";
import Massages from "../data_base/models/massages_model";

const chatRouter = require('koa-router')()

//необходимо будет заменить константы на перменные
chatRouter.prefix('/chat')

//функция работает, но не возвращает значение
/*async function jwtCheck(ctx){
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
        ctx.body = user
        })(ctx)
}*/

chatRouter.post('/', async ctx =>{
    const url = ctx.request.url;
    const lastDialogIndex = url.slice(url.indexOf('Index')+6, url.indexOf('&'))
    const status = url.slice(url.indexOf('status')+7, url.length)
        await passport.authenticate('jwt', { session: false },
            (error, user) => {
                ctx.body = user
            })(ctx)
        if (await ctx.body instanceof User){
            ctx.body = await getChatsFromParty(ctx.body.userName)
            ctx.body = await getChatsFromChat(ctx.body, lastDialogIndex, status)
        }
    }
)
chatRouter.post('/getMassages', async function (ctx) {
    const url = ctx.request.url;
    const chatId = url.slice(url.indexOf('chatId')+7, url.indexOf('&'));
    const earliestMassageIndex = url.slice(url.indexOf('Index')+6, url.length);
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
            ctx.body = user
        })(ctx)
    ctx.body = await getChatMassages(chatId, earliestMassageIndex)
})


chatRouter.post('/create', async function (ctx) {
    const url = ctx.request.url;
    const name = url.slice(url.indexOf('name')+5, url.indexOf('&')) //topic
    const clientName = decodeURI(url.slice(url.indexOf('clientName')+11, url.length))
    console.log('clientName=     '+clientName)
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
            ctx.body = user
        })(ctx)
    if (await ctx.body instanceof User){
        await createChat(ctx.body.userName, name, clientName)
        /*ctx.body = await getChatsFromParty(ctx.body.userName)
        ctx.body = await getChatsFromChat(ctx.body)*/
    }
})

chatRouter.post('/invite', async function (ctx) {
    const chatId = ctx.body.chatId
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
            ctx.body = user
        })(ctx)
    await inviteToChat(ctx.body.userName, chatId)
})
chatRouter.post('/sendMassage', async function (ctx) {
    const url = ctx.request.url
    const chatId = url.slice(url.indexOf('chatId')+7, url.indexOf('&content'))
    const content = url.slice(url.indexOf('content')+8, url.length)
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
            ctx.body = user
        })(ctx)
    await sendMassage(Number(chatId), ctx.body.userName, content)
    const massages = await Massages.query().where('chatId', chatId).where('content', content)
    ctx.body = massages[massages.length - 1]
})


chatRouter.post('/findMassages', async function (ctx) {
    const url = ctx.request.url
    const searchStr = url.slice(url.indexOf('searchStr')+10, url.length)
    await passport.authenticate('jwt', { session: false },
        (error, user) => {
            ctx.body = user
        })(ctx)
    ctx.body = await findMassages(ctx.body.userName, searchStr)
})
export default chatRouter;