import Koa from 'koa'
import bodyparser from 'koa-bodyparser'
import passport from './passport'
import UserRouter from './routes/users'
import chatRouter from './routes/chat'
import dbConnect, { knex } from './data_base/config/connectDB'
import cors from '@koa/cors'
import cookie from 'koa-cookie'
const app = new Koa()
app.use(cors())

app.use(cookie())

//пример работы с cookie
/*const abc = router();
abc.get('/', setACookie);

function setACookie() {
  this.cookie.set('foo', 'bar', {httpOnly: false});
}

app.use(abc.routes());*/
//
try {
  dbConnect
} catch (e) {
  console.log(e)
}

app.use(bodyparser({
  enableTypes:['json', 'form', 'text']
}));

app.use(passport.initialize())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});
app.use(UserRouter.routes(), UserRouter.allowedMethods())
app.use(chatRouter.routes(), chatRouter.allowedMethods())

module.exports = app;