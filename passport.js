import passport from 'koa-passport'
import { Strategy as LocalStrategy} from 'passport-local'
import { Strategy as JwtStrategy} from 'passport-jwt'
import {Strategy as GoogleStrategy} from'passport-google-oauth20'
import { Strategy as VKontakteStrategy} from 'passport-vkontakte';

import User from './data_base/models/user_model'
import jwt from 'jsonwebtoken'
import {ExtractJwt} from 'passport-jwt'


passport.use(new LocalStrategy({
  usernameField : 'email',
  passwordField : 'password',
  session: false
},
  async (username, password, done) => {
    let user = await User.query().findOne({ email: username })
    if (!user) {
      return done({ message: 'Incorrect username.' }, false)
    }
    if (user.password !== password) {
      return done({ message: 'Incorrect password.' }, false)
    }
    user=jwt.sign({ username }, 'secret')
    return done(null, user)
  }
))

passport.use(new JwtStrategy({
  //secretOrKey или secretOrKeyProvider(request, rawJwtToken, done)
  secretOrKey : 'secret',
  jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (jwt_payload, done) => {
    const user = await User.query().findOne({email: (jwt_payload.username !== undefined)? jwt_payload.username: jwt_payload.email})
    if (!user) {
      //выполняется
      return done({ message: 'Incorrect username.' }, false)
    }
    return done(null, user)
    }
))

//пoogle key AIzaSyCzKj124n-Sv5n3uUQg7dmmeVVORtA1nBI
//client id 903713015118-m5tjv2qctejanld5ua6e1qv9c5cna599.apps.googleusercontent.com
//secret PKopLtkNtxOtFcE3XZeY1tzY
passport.use(new GoogleStrategy({
            clientID: '903713015118-m5tjv2qctejanld5ua6e1qv9c5cna599.apps.googleusercontent.com',
        clientSecret: 'PKopLtkNtxOtFcE3XZeY1tzY',
        callbackURL: "/users"
    },
    function(accessToken, refreshToken, profile, cb) {
        User.findOrCreate({ googleId: profile.id }, function (err, user) {
            return cb(err, user);
        });
    }
));

passport.use(new VKontakteStrategy({
        clientID:     '7121214', // VK.com docs call it 'API ID', 'app_id', 'api_id', 'client_id' or 'apiId'
        clientSecret: 'lPc9y048nHx5ssyL35wE',
        callbackURL:  'http://localhost:3000/users/bar'
    },
    function(accessToken, refreshToken, params, profile, done) {
        User.findOrCreate({ vkontakteId: profile.id }, function (err, user) {
            return done(err, user);
        });
    }
));

export default passport