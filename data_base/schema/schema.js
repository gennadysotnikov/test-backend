import knex from '../config/connectDB';
//import {TableBuilder as table} from 'knex';



export  default async function createSchema() {
  //Сначала получаем ответ из базы, существует ли таблица с таким именем?
  //knex.schema.dropTable('users')
  if (await knex.schema.hasTable('users')){
    return //Возвращаем promise
  }
  await knex.schema.createTable('users', tableBuilder => {
    tableBuilder.increments('id').primary();
    tableBuilder.string('email');
    tableBuilder.string('userName');
    tableBuilder.string('password');
  }
  )
}
