import { Model } from 'objection'
import Knex from 'knex';


// Initialize knex.
export const knex = Knex({
  client: 'pg',
  useNullAsDefault: true,
  connection: {
    filename: 'users.db',
    password: 'noorsoft'
  },
});
Model.knex(knex)
export default knex
