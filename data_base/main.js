import knex from './config/connectDB';
import User from './models/user_model';
import createSchema from './schema/schema';


const main = async function main() {
  await createSchema()
  //точно известно, что таблица users с полями id, userName, email и password существует.
  /*const first = await User.query().insert({
    email: 'gena@noorsoft.ru', userName: 'gena', password: '1234'
  })
  const user=await User.query()
  console.log(user[1] instanceof User) // --> true
  console.log('there are', user.length, 'People in total')*/
}
export default main