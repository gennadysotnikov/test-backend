exports.up = function(knex) {
  return Promise.all([
    knex.schema.createTable('chat', table => {
      table.increments('chatId').primary()
      table.string('name')
      table.string('userName').references('userName').inTable('users').notNullable()
    }),
    knex.schema.createTable('party', table => {
      table.increments('id').primary()
      table.string('userName').references('userName').inTable('users').notNullable()
      table.integer('chatId').references('chatId').inTable('chat').notNullable()})
    ,
    knex.schema.createTable('massages', table => {
      table.increments('massageId').primary()
      table.string('userName').references('userName').inTable('users').notNullable()
      table.integer('chatId').references('chatId').inTable('chat').notNullable()
      table.string('content').notNullable()
      table.dateTime('dateCreate').nullable()})
    ,
    knex.schema.createTable('massageStatus', table => {
      table.increments('id').primary()
      table.integer('massageId').references('massageId').inTable('massages').notNullable()
      table.string('userName').references('userName').inTable('users').notNullable()
      table.boolean('isRead').nullable()})
  ]);
};
exports.down = function(knex) {
  return Promise.all([
    knex.schema.dropTableIfExists('massageStatus'),
    knex.schema.dropTableIfExists('party'),
    knex.schema.dropTableIfExists('massages'),
    knex.schema.dropTableIfExists('chat')
  ])
};
