
exports.up = function(knex) {
  return knex.schema.alterTable('chat', function (table) {
      table.string('status', 10).defaultTo('active').notNullable()
  })
};

exports.down = function(knex) {
    return knex.schema.alterTable('chat', function (table) {
        table.dropColumn('status')
    })
};
