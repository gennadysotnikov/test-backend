
exports.up = function(knex) {
    return knex.schema.alterTable('chat', function (table) {
        table.string('clientName', 40).defaultTo('Без имени').notNullable()
    })
};

exports.down = function(knex) {
    return knex.schema.alterTable('chat', function (table) {
        table.dropColumn('clientName')
    })
};
