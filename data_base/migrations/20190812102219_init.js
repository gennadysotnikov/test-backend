exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('users', table => {
            table.increments('id').primary()
            table.string('email').notNullable().unique()
            table.string('userName').notNullable().unique()
            table.string('password').notNullable()
        })]);
};
exports.down = function(knex) {
    return Promise.all([knex.schema.dropTableIfExists('users')])
};