import User from "./models/user_model";
import Party from "./models/party_model"
import Chat from "./models/chat_model"
import Massages from "./models/massages_model";
//Получить все диалоги для страницы

export async function getChatsFromParty( userName ){
    const parties =await Party.query().select().where( 'userName',userName)
    return parties
}
export async function getUserMassages( userName ){
    const parties =await Massages.query().select().where( 'userName',userName)
    return parties
}
export async function getChatMassages( chatId, earliestMassageIndex ){
    /*при повторном обновлении с клиента нужно посылать id последнего сообщения*/
    let massages
    massages = (earliestMassageIndex == 0)?
        Array.from(await Massages.query()
            .select()
            .orderBy('massageId','DESC')
            .where( 'chatId',chatId)
            .limit(10))
        .reverse():
        Array.from(await Massages.query()
            .select()
            .orderBy('massageId','DESC')
            .where( 'chatId',chatId)
            .where('massageId','<', earliestMassageIndex)
            .limit(10)
        )
        .reverse()
    return massages
}
export async function getChatsFromChat( chatsInParty, lastDialogIndex, status ) {
    let chats = []
    chatsInParty = chatsInParty.reverse()
    for(let index in chatsInParty) {
        if (chats.length<6)
        if (Number.parseInt(lastDialogIndex) ===0) {
            const chat = await Chat.query().findById(chatsInParty[index].chatId)
            if(chat.status === status) chats.push(chat)
        }
        else if (Number.parseInt(lastDialogIndex) > chatsInParty[index].chatId) {
            const chat = await Chat.query().findById(chatsInParty[index].chatId)
            if(chat.status === status) chats.push(chat)
        }

    }
    return chats
}

export async function createChat( userName, topic, clientName ){
    console.log('userName='+userName+' topic='+topic+' clientName='+clientName)
    await Chat.query().insert({name:topic, userName: userName, clientName:clientName})
    let chatId = await Chat.query().select('chatId').where('userName', userName)
    chatId = chatId[chatId.length-1].chatId
    await Party.query().insert({ chatId:chatId, userName:userName })
}

export async function inviteToChat( userName, chatId ){
    const users = await Party.query().select('userName').where('userName', userName).where('chatId', chatId)
    if (!(users[0] instanceof Party))
    {
        await Party.query().insert({ chatId:chatId, userName:userName })
    }
}

export async function sendMassage( chatId, userName, content ){
    const date = new Date()
    await Massages.query().insert({ chatId:chatId, userName: userName, content: content, dateCreate: date })
}

/*const chatIdFromParty = async (chatsInPartyElement, searchStr) => {
    await chatInChatTable = await Chat.query().findById(chatsInPartyElement.chatId)

    if (regExp.test(chatInChatTable.name)){

    }
}*/
// . - любой символ, кроме новой строки

export async function findMassages( userName, searchStr ){
    let nameMatches = []
    let massageMatches = []
    let userNameMatches = []
    const regExp = new RegExp(searchStr,'i')
    //поиск по названию чата
    const chatsInParty = await getChatsFromParty(userName) //получаем id всех чатов в которых состоит пользователь
    const chatsInChat = await getChatsFromChat(chatsInParty)
    for (let chatIndex in chatsInChat)
    {
        if (regExp.test(chatsInChat[chatIndex].name))
        {
            nameMatches.push(chatsInChat[chatIndex])
        }
    }
    //поиск по сообщениям
    const userMassages = await getUserMassages( userName )
    for (let massageIndex in userMassages)
    {
        if (regExp.test(userMassages[massageIndex].content))
        {
            massageMatches.push(userMassages[massageIndex])
        }
    }
    //поиск по userName
    const allUsers = await User.query()
    for (let userIndex in allUsers)
    {
        if (regExp.test(allUsers[userIndex].userName))
        {
            userNameMatches.push({username:allUsers[userIndex].userName})
        }
    }
    return {nameMatches, massageMatches, userNameMatches}
}
