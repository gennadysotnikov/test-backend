import {Model} from 'objection'
import User from './user_model'
import Chat from './chat_model'


class Massages extends Model{
    static get tableName() {
        return 'massages';
    }

    static get idColumn() {
        return 'massageId';
    }
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['chatId', 'userName','content','dateCreate'],
            properties: {
                massageId: {type: 'integer'},
                userName: {type: 'string', minLength: 1, maxLength: 255},
                chatId: {type: 'integer'},
                content: {type: 'string', minLength: 1, maxLength: 255},
                dateCreate:{ type: 'dateTime' }
            }
        }
    }
    static get relationMappings() {
        // Importing models here is a one way to avoid require loops.
        return {
            sender: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'users.userName',
                    to: 'massages.userName'
                }
            },
            chat: {
                relation: Model.BelongsToOneRelation,
                modelClass: Chat,
                join: {
                    from: 'Chat.chatId',
                    to: 'massages.chatId'
                }
            }
        };
    }
}

export default Massages;
