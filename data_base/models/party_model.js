import { Model } from "objection"
import User from './user_model'
import Chat from './chat_model'

class Party extends Model{
    static get tableName() {
        return 'party';
    }

    static get idColumn() {
        return 'id';
    }
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['chatId', 'userName'],
            properties: {
                id: {type: 'integer'},
                userName: {type: 'string', minLength: 1, maxLength: 255},
                chatId: {type: 'integer'}
            }
        }
    }
    static get relationMappings() {
        // Importing models here is a one way to avoid require loops.
        return {
            participant: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'users.userName',
                    to: 'party.userName'
                }
            },
            chatOfParty: {
                relation: Model.BelongsToOneRelation,
                modelClass: Chat,
                join: {
                    from: 'chat.chatId',
                    to: 'party.chatId'
                }
            }
        };
    }
}
export default Party;
