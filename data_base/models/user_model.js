import { Model } from 'objection'
import jwt from "jsonwebtoken"



class User extends Model {
  static get tableName() {
    return 'users';
  }
  static get idColumn() {
    return 'id'
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email'||'userName', 'password'],
      properties: {
        id: {type: 'integer'},
        email:{type: 'string', minLength: 1, maxLength: 255},
        userName: {type: 'string', minLength: 1, maxLength: 255},
        password: {type: 'string', minLength: 1, maxLength: 255}
      }
    }
  }
  static async registration(email, userName, password){
    await User.query().insert({email: email, userName: userName, password: password})
  }
  static async isUser(email){
    if ((await User.query().where('email', email))[0] instanceof User){
      return jwt.sign({email}, 'secret');
    }
    else {
      return null;
    }
  }
  static async findOrCreate(userData, cb){
    let user = await this.query().findById(userData.id)
    if (!user)
    {
      console.log('Пользователь не найден. Будет создан пользователь user user@user.ru с паролем 1234')
      await this.query().insert({
        id:userData.id,email: 'user@user.ru', userName: 'user', password: '1234'})
      user = await this.query().findById(userData.id)
    }
    await cb()
  }
}
export default User