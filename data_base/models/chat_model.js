import {Model} from "objection"
import User from './user_model'

class Chat extends Model{
    static get tableName() {
        return 'chat'
    }

    static get idColumn() {
        return 'chatId'
    }
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['userName'],
            properties: {
                chatId: {type: 'integer'},
                name:{type: 'string', minLength: 1, maxLength: 255},
                userName: {type: 'string', minLength: 1, maxLength: 255},
            }
        }
    }
    static get relationMappings() {
        // Importing models here is a one way to avoid require loops.
        return {
            chatCreator: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'chat.userName',
                    to: 'users.userName'
                }
            }
        }
    }
}

export default Chat
