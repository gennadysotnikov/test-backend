import {Model} from "objection";


class MassageStatus extends Model{
    static get tableName() {
        return 'party';
    }

    static get idColumn() {
        return 'id';
    }
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['userName', 'massageId','isRead'],
            properties: {
                id: {type: 'integer'},
                userName: {type: 'string', minLength: 1, maxLength: 255},
                chatId: {type: 'integer'},
                isRead: { type:'boolean'}
            }
        }
    }
    static get relationMappings() {
        // Importing models here is a one way to avoid require loops.
        const User = require('./user_model');
        const Massages = require('./massages_model');
        return {
            userName: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'users.userName',
                    to: 'massageStatus.userName'
                }
            },
            massageId: {
                relation: Model.BelongsToOneRelation,
                modelClass: Massages,
                join: {
                    from: 'massages.massageId',
                    to: 'massageStatus.massageId'
                }
            }
        };
    }
}
export default MassageStatus;
