module.exports = {
  development: {
    client: 'pg',
    useNullAsDefault: true,
    connection: {
      filename: 'users.db',
      password: 'noorsoft'
    },
    migrations: {
      directory: 'data_base/migrations',
    },
  },
};